  testRequestSpeed = function(url, callback) {
    var st;
    st = Date.now();
    return jQuery.ajax({
      type: "GET",
      url: url + '',
      processData: true,
      data: {
        rnd: Math.random()
      },
      success: function(data, textStatus, jqXHR) {
        var duration, rate, size;
        size = parseInt(jqXHR.getResponseHeader('content-length'));
        duration = Date.now() - st;
        rate = (size / duration).toFixed(2);
        return callback(duration, rate);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        var duration, rate, size;
        if (jqXHR.status === 0) {
          return callback(-1, -1);
        } else {
          size = parseInt(jqXHR.getResponseHeader('content-length'));
          duration = Date.now() - st;
          rate = (size / duration).toFixed(2);
          return callback(duration, rate);
        }
      }
    });
  };